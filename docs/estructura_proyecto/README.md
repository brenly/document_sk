# Estrucutra del código Fuente

El proyecto se encuentra actualmente estructurada bajo la arquitectura por capas como se muestra a continuación:  
![arquitectura por capas](arquitectura_capas.png)  

## Arborescencia de directorios:

```
.
├── gulpfile.js
├── index.js                            # archivo de inicio para el sistema
├── package.json
├── public                              # Recursos publicos (js, css, imagenes) 
│   ├── css
│   │   └── style.css
│   │   ├── ...
│   ├── img
│   │   ├── ciudadania-digital.png
│   │   ├── ...
│   └── js
│       ├── main.js
│       ├── ...
├── src                                 # carpeta raiz del código fuente
│   ├── common                          # recursos comunes
│   │   ├── cttes.js                    # constantes del sistema
│   │   └── lib                         # librerias
│   │       └── util.js
│   │       ├── ...
│   ├── config                          # archivos de configuracion del sistema
│   │   ├── configuration.js.sample
│   │   ├── ...
│   ├── controllers                     # capa de controladores (bussines delegate)
│   │   ├── clients.controller.js
│   │   ├── ...
│   ├── db.js                           # archivo de inicio de la bd dca_server_db
│   ├── domain                          # capa de logica de negocio
│   │   ├── clients
│   │   └── ...
│   ├── middlewares                     # capa de middlewares
│   │   ├── auth.middleware.js
│   │   ├── ...
│   ├── models                          # capa dao
│   │   ├── clients.js
│   │   ├── ...
│   ├── routes                          # capa de presentacion
│   │   ├── clients.route.js
│   │   ├── ...
│   ├── scripts                         # scripts de migracion
│   │   └── migrations.js
│   ├── seeders                         # datos que inician el sistema
│   │   ├── scopes.seeders.js
│   │   └── ...
│   ├── sequelize.js                    # archivo de inicio de la bd dca_server_db
│   ├── services                        # capa de servicios
│   │   ├── index.js
│   │   └── ...
│   └── setup.js                        # archivo la creacion de las tablas y cargado de datos iniciales
├── tailwind.config.js
├── test                                # test del sistema
│   ├── int                             # test de integración
│   │   └── client.int.test.js          
│   │   └── ...
│   └── unit                            # test unitarios
│       ├── account.test.js
│       ├── ...
└── views                               # vistas (ejs)
    ├── _layout.ejs
    ├── ...
```

