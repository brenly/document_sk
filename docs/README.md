# “SISTEMA DE GESTIÓN INTEGRADO DE REMUNERACIONES” - SISTEMA KEPLER v1.0 - Documentación


De acuerdo al D.S. N° 29894 de 7 de febrero de 2009 "Estructura Organizativa del Órgano Ejecutivo del Estado Plurinacional de Bolivia”, se define la estructura orgánica referente al MEFP, en la que se incluye jerárquicamente al VTCP y a la DGPOT que entre las atribuciones descritas en el mencionado Decreto; son los encargados de la formulación e implementación de políticas económicas soberanas en el marco de la gestión pública, así como la distribución eficiente de los recursos del TGN en la gestión de la política fiscal y la estabilidad macroeconómica, acorde con los principios y valores del Estado Plurinacional (UAIS, Manual de la Calidad).La UAIS dependiente de la DGPOT, como se muestra en el documento AX- 001 Estructura Organizacional de la UAIS, del manual de la calidad de su SGC implantado, es catalogada como unidad de tipo sustantivo; comparte dependencia con cuatro unidades del mismo nivel, tiene relaciones Intra Institucionales con otras unidades y/o áreas organizacionales del MEFP, y se relaciona de forma Inter Institucional con las entidades del sector público que tengan registrado sus recursos financieros disponibles para su ejecución en la CUT (UAIS, Manual de la Calidad). 
La UAIS sustenta las gestiones encomendadas, en el marco del inciso “t” del Artículo 56 del D.S. N° 29894, que señala que una de sus atribuciones es la de “Centralizar la información de planilla de haberes, rentas y pensiones de las servidoras y servidores públicos”.





## Tabla de contenidos
### 


1. [Especificación de casos de uso](#)
2. [Diagrama de casos de uso](#diagrama_casos_uso)
3. [Diagrama de secuencia](#diagrama_secuencia)
4. [Diagrama de flujo](#diagrama_flujo)
5. [Diagrama de estados](#diagrama_estados)
6. [Diagramas de base de datos](#diagramas_base_datos)
7. [Diagramas de arquitectura](#diagramas_arquitectura)
8. [Estructura del código Fuente](estructura_proyecto/)

## Diagrama de casos de uso<a name="diagrama_casos_uso"></a>
![diagrama de casos de uso](analisis/casos_uso/diagrama_casos_uso.png)  

## Diagrama de secuencia<a name="diagrama_secuencia"></a>

### Flujo  `authorization_code`  

Los clientes confidenciales utilizan el tipo de concesión del Código de autorización para intercambiar un código de autorización por un token de acceso.  

Una vez que el usuario regresa al cliente a través de la URL de redireccionamiento, la aplicación obtendrá el código de autorización de la URL y lo usará para solicitar un token de acceso.  

![diagrama de secuencia authorization_code](analisis/diagramas_secuencia/diagrama_secuencia_authorization_code.jpg)  

###  Flujo  `client_credentials`  

Los clientes utilizan el tipo de concesión de Credenciales de cliente para obtener un token de acceso fuera del contexto de un usuario.  

![diagrama de secuencia client_credentials](analisis/diagramas_secuencia/diagrama_secuencia_client_credentials.jpg)  

### Flujo  `authorization_code + pkce`  

Los clientes nativos utilizan el tipo de concesión del Código de autorización + pKCE para intercambiar un código de autorización por un token de acceso.  

Una vez que el usuario regresa al cliente a través de la URL de redireccionamiento, la aplicación obtendrá el código de autorización del activity y lo usará para solicitar un token de acceso.  

![diagrama de secuencia client_credentials + pkce](analisis/diagramas_secuencia/diagrama_secuencia_pkce.jpg)  

## Diagrama de flujo<a name="diagrama_flujo"></a>
Diagrama de flijo para el inicio de sesión de un usuario en un sistema cliente.

![Diagrama de flujo](analisis/diagramas_flujo/diagrama_flujo.png)  

## Diagrama de estados<a name="diagrama_estados"></a>
Diagrama de estados para la tabla `users`
![Diagrama de estados](analisis/diagramas_estado/user_status.png)  

### Descripción de los estados

**START**: Estado inicial.

**SELF_REGISTER**: El usuario realizo su registro pero no realizo la activación de la cuenta.

**REMOVED**: La cuenta fue eliminada porque esta no fue activada o por un operador (solo se eliminan cuentas que no han sido activadas).

**ACTIVE**: El usuario realizo la activación de la cuenta.

**SUSPENDED**: La cuenta fue suspendida a solicitud del usuario o por inactividad. 

**OBSERVED**: La cuena fue observada, caso particular.

**PENDING_INACTIVE**: Se solicito la baja de la cuenta y esta sera dado de baja en `X` dias.

**INACTIVE**: Cuenta dada de baja.

## Diagramas de base de datos<a name="diagramas_base_datos"></a>

### id_server_db
![Diagramas de base de datos id_server_db](analisis/bd/ID_SERVER.svg)  

### Descripción de las tablas

**Esta base de datos utiliza el core de la librería que habilita el acceso a OAuth 2 y OpenId Connect, por lo que se hará la descripción de las tablas que se hacen uso para cumplir con los requerimientos del Proveedor de Identidad de Ciudadanía Digital.**


**AccessTokens**: Almacena los tokens de acceso generados para un sistema cliente.

**AuthorizationCodes**: Almacena el código de autorización intermedio generado en el flujo de OAuth 2.0 Authorization Code.

**Clients**: Almacena la información de los sistemas cliente.

**Sessions**: Almacena la información de las sesiones web

### dca_server_db

![Diagramas de base de datos dca_server_db](analisis/bd/DCA_SERVER.svg)  

### Descripción de las tablas

**users**: Tabla donde se almacenan los datos de las cuentas de usuario por cada ciudadano.

**clients**: Tabla donde se almacenan los datos de los sistemas clientes que hacen uso del proveedor de identidad como mecanismo de autenticación.

**user_clients**: Tabla donde se almacenan los scopes que el usuario ha autorizado por sistema cliente.

**devices**: Tabla donde se almacenan los dispositivos móviles que el ciudadano ha activado a través de la aplicación móvil.
 
**sessions**: Tabla donde se almacenan las sesiones de los usuarios.

**scopes**: Tabla donde se almacenan los scopes permitidos.

**white_list_clients**: Tabla donde se almacenan clientes nativos y clientes b2b.

**white_list_clients_scopes**: Tabla donde se almacenan los scopes permitidos a los clientes almacenados en la tabla `white_list_clients`.

**two_factors**: Tabla donde se almacenan los segundos factores utilizados.

**control_locks**: Tabla donde se almacenan los intentos por error del usuario.

**users_locks**: Tabla donde se almacenan los bloqueos del usuario.

**user_two_factors**: Tabla donde se almacenan los segundos factor preferido del usuario.

**well_know_browsers**: Tabla donde se almacenan los navegadores conocidos del usuario.

**update_interaction**: Tabla donde se almacenan las solicitudes de actualización de tatos del usuario.

**log_audits**: Almacena el histórico de cada cambio tabla users.

## Diagramas de arquitectura<a name="diagramas_arquitectura"></a>

Los arquitectura del proyecto se elaboro en base al modelo `C4`.

Los diagramas de `contexto` y `contenedores` estan en el repositorio del proyecto [ciudadania digital](#idx)

### Diagrama de componentes
![diagramas de arquitectura - componentes](analisis/arquitectura/C4/C4_Components.svg)  