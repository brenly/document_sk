# “SISTEMA DE GESTIÓN INTEGRADO DE REMUNERACIONES” - SISTEMA KEPLER v1.0

El `Proveedor de Identidad de Ciudadanía Digital` es la plataforma de autenticación implementada en el marco de lo establecido en la [Ley 1080 de Ciudadanía Digital](https://digital.gob.bo/2019/05/ley-de-ciudadania-digital-1080/).

Está desarrollado sobre [OpenID Connect 1.0](https://openid.net/specs/openid-connect-core-1_0.html), que es un protocolo de identidad simple y de estándar abierto creado sobre el protocolo [OAuth 2.0](https://tools.ietf.org/html/rfc6749), el cual permite a aplicaciones clientes adscribirse y presentar a sus usuarios un único mecanismo de autenticación y autorización.


## Tecnologías

* NodeJS 12.x.x
* Koa 2.11.0
* Ejs 2.5.6
* Tailwindcss 1.5.2
* Redis
* Postgres 11

## Instalación

* [Manual de instalación](INSTALL.md)  

## Documentación

* [Documentación del proyecto](docs/)    

## Especificaciones técnicas de implementación
* [Especficaciones técnicas](docs/protocolos/)


